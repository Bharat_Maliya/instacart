export const shopdata = [
  {
    "name": "My Groceries",
    "image": "https://www.instacart.com/image-server/100x/www.instacart.com/assets/domains/warehouse/logo/1/1fe0065e-a947-4b5d-b274-3900694536d5.png",
    "categories": [
      {
        "name": "Fruits",
        "items": [
          {
            "name": "Apples",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?apples,fruit"
          },
          {
            "name": "Oranges",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?oranges,fruit"
          },
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,fruit"
          },
          {
            "name": "Pineapples",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?pineapples,fruit"
          },
          {
            "name": "Strawberries",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?strawberries,fruit"
          },
          {
            "name": "Grapes",
            "price": 2.79,
            "image": "https://source.unsplash.com/800x600/?grapes,fruit"
          },
          {
            "name": "Blueberries",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?blueberries,fruit"
          },
          {
            "name": "Raspberries",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?raspberries,fruit"
          },
          {
            "name": "Mangoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?mangoes,fruit"
          },
          {
            "name": "Pears",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?pears,fruit"
          },
          {
            "name": "Watermelons",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?watermelons,fruit"
          },
          {
            "name": "Cantaloupes",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cantaloupes,fruit"
          },
          {
            "name": "Honeydews",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?honeydews,fruit"
          },
          {
            "name": "Lemons",
            "price": 1.29,
            "image": "https://source.unsplash.com/800x600/?lemons,fruit"
          },
          {
            "name": "Limes",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?limes,fruit"
          },
          {
            "name": "Grapefruits",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?grapefruits,fruit"
          },
          {
            "name": "Kiwi",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?kiwi,fruit"
          },
          {
            "name": "Plums",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?plums,fruit"
          },
          {
            "name": "Nectarines",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?nectarines,fruit"
          },
          {
            "name": "Apricots",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?apricots,fruit"
          },
          {
            "name": "Clementines",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?clementines,fruit"
          }
        ]
      },
      {
        "name": "Vegetables",
        "items": [
          {
            "name": "Carrots",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?carrots,vegetables"
          },
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,vegetables"
          },
          {
            "name": "Cauliflower",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?cauliflower,vegetables"
          },
          {
            "name": "Tomatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?tomatoes,vegetables"
          },
          {
            "name": "Lettuce",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?lettuce,vegetables"
          },
          {
            "name": "Spinach",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?spinach,vegetables"
          },
          {
            "name": "Kale",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?kale,vegetables"
          },
          {
            "name": "Peppers",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?peppers,vegetables"
          },
          {
            "name": "Potatoes",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,vegetables"
          },
          {
            "name": "Onions",
            "price": 1.29,
            "image": "https://source.unsplash.com/800x600/?onions,vegetables"
          },
          {
            "name": "Garlic",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?garlic,vegetables"
          },
          {
            "name": "Mushrooms",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mushrooms,vegetables"
          },
          {
            "name": "Cucumbers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cucumbers,vegetables"
          },
          {
            "name": "Zucchini",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?zucchini,vegetables"
          },
          {
            "name": "Eggplant",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?eggplant,vegetables"
          },
          {
            "name": "Artichokes",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?artichokes,vegetables"
          },
          {
            "name": "Asparagus",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?asparagus,vegetables"
          },
          {
            "name": "Green beans",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?greenbeans,vegetables"
          },
          {
            "name": "Peas",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?peas,vegetables"
          },
          {
            "name": "Corn",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?corn,vegetables"
          },
          {
            "name": "Beets",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?beets,vegetables"
          },
          {
            "name": "Pumpkin",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?pumpkin,vegetables"
          },
          {
            "name": "Sweet potato",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?sweetpotato,vegetables"
          }
        ]
      },
      {
        "name": "Meat",
        "items": [
          {
            "name": "Beef",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?beef,meat"
          },
          {
            "name": "Chicken",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?chicken,meat"
          },
          {
            "name": "Pork",
            "price": 8.49,
            "image": "https://source.unsplash.com/800x600/?pork,meat"
          },
          {
            "name": "Lamb",
            "price": 11.99,
            "image": "https://source.unsplash.com/800x600/?lamb,meat"
          }
        ]
      },
      {
        "name": "Seafood",
        "items": [
          {
            "name": "Salmon",
            "price": 12.99,
            "image": "https://source.unsplash.com/800x600/?salmon,seafood"
          },
          {
            "name": "Shrimp",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?shrimp,seafood"
          },
          {
            "name": "Crab",
            "price": 16.99,
            "image": "https://source.unsplash.com/800x600/?crab,seafood"
          },
          {
            "name": "Lobster",
            "price": 32.99,
            "image": "https://source.unsplash.com/800x600/?lobster,seafood"
          }
        ]
      },
      {
        "name": "Dairy",
        "items": [
          {
            "name": "Milk",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?milk,dairy"
          },
          {
            "name": "Cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cheese,dairy"
          },
          {
            "name": "Butter",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?butter,dairy"
          },
          {
            "name": "Yogurt",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?yogurt,dairy"
          },
          {
            "name": "Cream",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?cream,dairy"
          }
        ]
      },
      {
        "name": "Bakery",
        "items": [
          {
            "name": "Bread",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?bread,bakery"
          },
          {
            "name": "Croissant",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?croissant,bakery"
          },
          {
            "name": "Muffin",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?muffin,bakery"
          },
          {
            "name": "Bagel",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?bagel,bakery"
          },
          {
            "name": "Cake",
            "price": 15.99,
            "image": "https://source.unsplash.com/800x600/?cake,bakery"
          }
        ]
      }
    ]
  },
  {
    "name": "Fresh Market",
    "image": "https://www.instacart.com/image-server/100x/www.instacart.com/assets/domains/warehouse/logo/5/65f2304b-908e-4cd0-981d-0d4e4effa8de.png",
    "categories": [
      {
        "name": "Produce",
        "items": [
          {
            "name": "Apples",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?apples,produce"
          },
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,produce"
          },
          {
            "name": "Carrots",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?carrots,produce"
          },
          {
            "name": "Lettuce",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lettuce,produce"
          },
          {
            "name": "Tomatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?tomatoes,produce"
          },
          {
            "name": "Cucumbers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cucumbers,produce"
          },
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,produce"
          },
          {
            "name": "Grapes",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?grapes,produce"
          },
          {
            "name": "Peppers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?peppers,produce"
          },
          {
            "name": "Oranges",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?oranges,produce"
          },
          {
            "name": "Onions",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?onions,produce"
          },
          {
            "name": "Potatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,produce"
          },
          {
            "name": "Avocados",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?avocados,produce"
          },
          {
            "name": "Strawberries",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?strawberries,produce"
          },
          {
            "name": "Mushrooms",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mushrooms,produce"
          },
          {
            "name": "Pineapple",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?pineapple,produce"
          },
          {
            "name": "Kale",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?kale,produce"
          },
          {
            "name": "Cabbage",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cabbage,produce"
          },
          {
            "name": "Corn",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?corn,produce"
          },
          {
            "name": "Zucchini",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?zucchini,produce"
          }
        ]
      },
      {
        "name": "Meat",
        "items": [
          {
            "name": "Beef",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?beef,meat"
          },
          {
            "name": "Chicken",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?chicken,meat"
          },
          {
            "name": "Pork",
            "price": 7.99,
            "image": "https://source.unsplash.com/800x600/?pork,meat"
          },
          {
            "name": "Lamb",
            "price": 12.99,
            "image": "https://source.unsplash.com/800x600/?lamb,meat"
          },
          {
            "name": "Salmon",
            "price": 10.99,
            "image": "https://source.unsplash.com/800x600/?salmon,meat"
          },
          {
            "name": "Shrimp",
            "price": 8.99,
            "image": "https://source.unsplash.com/800x600/?shrimp,meat"
          },
          {
            "name": "Ground beef",
            "price": 8.99,
            "image": "https://source.unsplash.com/800x600/?ground-beef,meat"
          },
          {
            "name": "Tuna",
            "price": 11.99,
            "image": "https://source.unsplash.com/800x600/?tuna,meat"
          },
          {
            "name": "Crab",
            "price": 14.99,
            "image": "https://source.unsplash.com/800x600/?crab,meat"
          },
          {
            "name": "Cod",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?cod,meat"
          },
          {
            "name": "Turkey",
            "price": 7.99,
            "image": "https://source.unsplash.com/800x600/?turkey,meat"
          },
          {
            "name": "Halibut",
            "price": 15.99,
            "image": "https://source.unsplash.com/800x600/?halibut,meat"
          },
          {
            "name": "Sausage",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?sausage,meat"
          },
          {
            "name": "Bacon",
            "price": 5.99,
            "image": "https://source.unsplash.com/800x600/?bacon,meat"
          },
          {
            "name": "Lobster",
            "price": 25.99,
            "image": "https://source.unsplash.com/800x600/?lobster,meat"
          },
          {
            "name": "Veal",
            "price": 14.99,
            "image": "https://source.unsplash.com/800x600/?veal,meat"
          },
          {
            "name": "Duck",
            "price": 13.99,
            "image": "https://source.unsplash.com/800x600/?duck,meat"
          },
          {
            "name": "Pheasant",
            "price": 18.99,
            "image": "https://source.unsplash.com/800x600/?pheasant,meat"
          },
          {
            "name": "Quail",
            "price": 17.99,
            "image": "https://source.unsplash.com/800x600/?quail,meat"
          }
        ]
      },
      {
        "name": "Bakery",
        "items": [
          {
            "name": "Bread",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?bread,bakery"
          },
          {
            "name": "Croissant",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?croissant,bakery"
          },
          {
            "name": "Bagel",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?bagel,bakery"
          },
          {
            "name": "Muffin",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?muffin,bakery"
          },
          {
            "name": "Pastry",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?pastry,bakery"
          },
          {
            "name": "Cupcake",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?cupcake,bakery"
          },
          {
            "name": "Cake",
            "price": 15.99,
            "image": "https://source.unsplash.com/800x600/?cake,bakery"
          },
          {
            "name": "Pie",
            "price": 12.99,
            "image": "https://source.unsplash.com/800x600/?pie,bakery"
          },
          {
            "name": "Donut",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?donut,bakery"
          },
          {
            "name": "Baguette",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?baguette,bakery"
          },
          {
            "name": "Sourdough bread",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?sourdough-bread,bakery"
          },
          {
            "name": "Pretzel",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?pretzel,bakery"
          },
          {
            "name": "Danish",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?danish,bakery"
          },
          {
            "name": "Focaccia",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?focaccia,bakery"
          },
          {
            "name": "Cinnamon roll",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?cinnamon-roll,bakery"
          },
          {
            "name": "Brioche",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?brioche,bakery"
          },
          {
            "name": "English muffin",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?english-muffin,bakery"
          },
          {
            "name": "Cornbread",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?cornbread,bakery"
          },
          {
            "name": "Scone",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?scone,bakery"
          },
          {
            "name": "Ciabatta",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?ciabatta,bakery"
          }
        ]
      },
      {
        "name": "Dairy",
        "items": [
          {
            "name": "Milk",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?milk,dairy"
          },
          {
            "name": "Butter",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?butter,dairy"
          },
          {
            "name": "Cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cheese,dairy"
          },
          {
            "name": "Yogurt",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?yogurt,dairy"
          },
          {
            "name": "Sour cream",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?sour-cream,dairy"
          },
          {
            "name": "Cream cheese",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?cream-cheese,dairy"
          },
          {
            "name": "Eggs",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?eggs,dairy"
          },
          {
            "name": "Whipping cream",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?whipping-cream,dairy"
          },
          {
            "name": "Half and half",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?half-and-half,dairy"
          },
          {
            "name": "Margarine",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?margarine,dairy"
          },
          {
            "name": "Cottage cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cottage-cheese,dairy"
          },
          {
            "name": "Almond milk",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?almond-milk,dairy"
          },
          {
            "name": "Soy milk",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?soy-milk,dairy"
          },
          {
            "name": "Heavy cream",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?heavy-cream,dairy"
          },
          {
            "name": "Goat cheese",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?goat-cheese,dairy"
          },
          {
            "name": "Feta cheese",
            "price": 5.99,
            "image": "https://source.unsplash.com/800x600/?feta-cheese,dairy"
          },
          {
            "name": "Buttermilk",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?buttermilk,dairy"
          },
          {
            "name": "Cottage cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cottage-cheese,dairy"
          },
          {
            "name": "Greek yogurt",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?greek-yogurt,dairy"
          },
          {
            "name": "Buttercream",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?buttercream,dairy"
          }
        ]
      },
      {
        "name": "Produce",
        "items": [
          {
            "name": "Apples",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?apples,produce"
          },
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,produce"
          },
          {
            "name": "Carrots",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?carrots,produce"
          },
          {
            "name": "Oranges",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?oranges,produce"
          },
          {
            "name": "Lemons",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?lemons,produce"
          },
          {
            "name": "Kale",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?kale,produce"
          },
          {
            "name": "Lettuce",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lettuce,produce"
          },
          {
            "name": "Spinach",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?spinach,produce"
          },
          {
            "name": "Tomatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?tomatoes,produce"
          },
          {
            "name": "Cucumbers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cucumbers,produce"
          },
          {
            "name": "Bell peppers",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?bell-peppers,produce"
          },
          {
            "name": "Zucchini",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?zucchini,produce"
          },
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,produce"
          },
          {
            "name": "Cauliflower",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?cauliflower,produce"
          },
          {
            "name": "Corn",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?corn,produce"
          },
          {
            "name": "Green beans",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?green-beans,produce"
          },
          {
            "name": "Onions",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?onions,produce"
          },
          {
            "name": "Potatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,produce"
          },
          {
            "name": "Sweet potatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?sweet-potatoes,produce"
          }
        ]
      }
    ]
  },
  {
    "name": "Green Grocers",
    "image": "https://www.instacart.com/image-server/100x/www.instacart.com/assets/domains/warehouse/logo/436/695aaa61-4dab-4965-a7e3-0c2f93e9b119.png",
    "categories": [
      {
        "name": "Vegetables",
        "items": [
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,vegetables"
          },
          {
            "name": "Carrots",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?carrots,vegetables"
          },
          {
            "name": "Cauliflower",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?cauliflower,vegetables"
          },
          {
            "name": "Spinach",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?spinach,vegetables"
          },
          {
            "name": "Kale",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?kale,vegetables"
          },
          {
            "name": "Lettuce",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lettuce,vegetables"
          },
          {
            "name": "Cabbage",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cabbage,vegetables"
          },
          {
            "name": "Peppers",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?peppers,vegetables"
          },
          {
            "name": "Tomatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?tomatoes,vegetables"
          },
          {
            "name": "Zucchini",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?zucchini,vegetables"
          },
          {
            "name": "Onions",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?onions,vegetables"
          },
          {
            "name": "Garlic",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?garlic,vegetables"
          },
          {
            "name": "Ginger",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?ginger,vegetables"
          },
          {
            "name": "Mushrooms",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mushrooms,vegetables"
          },
          {
            "name": "Potatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,vegetables"
          },
          {
            "name": "Sweet potatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?sweet-potatoes,vegetables"
          }
        ]
      },
      {
        "name": "Fruits",
        "items": [
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,fruits"
          },
          {
            "name": "Apples",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?apples,fruits"
          },
          {
            "name": "Oranges",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?oranges,fruits"
          },
          {
            "name": "Grapes",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?grapes,fruits"
          },
          {
            "name": "Lemons",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lemons,fruits"
          },
          {
            "name": "Limes",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?limes,fruits"
          },
          {
            "name": "Peaches",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?peaches,fruits"
          },
          {
            "name": "Pineapples",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?pineapples,fruits"
          },
          {
            "name": "Strawberries",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?strawberries,fruits"
          },
          {
            "name": "Blueberries",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?blueberries,fruits"
          },
          {
            "name": "Raspberries",
            "price": 4.49,
            "image": "https://source.unsplash.com/800x600/?raspberries,fruits"
          },
          {
            "name": "Watermelon",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?watermelon,fruits"
          },
          {
            "name": "Honeydew",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?honeydew,fruits"
          },
          {
            "name": "Mangoes",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mangoes,fruits"
          },
          {
            "name": "Pears",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?pears,fruits"
          },
          {
            "name": "Plums",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?plums,fruits"
          },
          {
            "name": "Cherries",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?cherries,fruits"
          },
          {
            "name": "Kiwi",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?kiwi,fruits"
          }
        ]
      },
      {
        "name": "Bakery",
        "items": [
          {
            "name": "Bread",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?bread,bakery"
          },
          {
            "name": "Croissant",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?croissant,bakery"
          },
          {
            "name": "Bagels",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?bagels,bakery"
          },
          {
            "name": "Donuts",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?donuts,bakery"
          },
          {
            "name": "Muffins",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?muffins,bakery"
          },
          {
            "name": "Cupcakes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cupcakes,bakery"
          }
        ]
      }
    ]
  },
  {
    "name": "My Groceries",
    "image": "https://www.instacart.com/image-server/100x/www.instacart.com/assets/domains/warehouse/logo/1/1fe0065e-a947-4b5d-b274-3900694536d5.png",
    "categories": [
      {
        "name": "Fruits",
        "items": [
          {
            "name": "Apples",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?apples,fruit"
          },
          {
            "name": "Oranges",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?oranges,fruit"
          },
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,fruit"
          },
          {
            "name": "Pineapples",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?pineapples,fruit"
          },
          {
            "name": "Strawberries",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?strawberries,fruit"
          },
          {
            "name": "Grapes",
            "price": 2.79,
            "image": "https://source.unsplash.com/800x600/?grapes,fruit"
          },
          {
            "name": "Blueberries",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?blueberries,fruit"
          },
          {
            "name": "Raspberries",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?raspberries,fruit"
          },
          {
            "name": "Mangoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?mangoes,fruit"
          },
          {
            "name": "Pears",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?pears,fruit"
          },
          {
            "name": "Watermelons",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?watermelons,fruit"
          },
          {
            "name": "Cantaloupes",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cantaloupes,fruit"
          },
          {
            "name": "Honeydews",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?honeydews,fruit"
          },
          {
            "name": "Lemons",
            "price": 1.29,
            "image": "https://source.unsplash.com/800x600/?lemons,fruit"
          },
          {
            "name": "Limes",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?limes,fruit"
          },
          {
            "name": "Grapefruits",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?grapefruits,fruit"
          },
          {
            "name": "Kiwi",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?kiwi,fruit"
          },
          {
            "name": "Plums",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?plums,fruit"
          },
          {
            "name": "Nectarines",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?nectarines,fruit"
          },
          {
            "name": "Apricots",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?apricots,fruit"
          },
          {
            "name": "Clementines",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?clementines,fruit"
          }
        ]
      },
      {
        "name": "Vegetables",
        "items": [
          {
            "name": "Carrots",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?carrots,vegetables"
          },
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,vegetables"
          },
          {
            "name": "Cauliflower",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?cauliflower,vegetables"
          },
          {
            "name": "Tomatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?tomatoes,vegetables"
          },
          {
            "name": "Lettuce",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?lettuce,vegetables"
          },
          {
            "name": "Spinach",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?spinach,vegetables"
          },
          {
            "name": "Kale",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?kale,vegetables"
          },
          {
            "name": "Peppers",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?peppers,vegetables"
          },
          {
            "name": "Potatoes",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,vegetables"
          },
          {
            "name": "Onions",
            "price": 1.29,
            "image": "https://source.unsplash.com/800x600/?onions,vegetables"
          },
          {
            "name": "Garlic",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?garlic,vegetables"
          },
          {
            "name": "Mushrooms",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mushrooms,vegetables"
          },
          {
            "name": "Cucumbers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cucumbers,vegetables"
          },
          {
            "name": "Zucchini",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?zucchini,vegetables"
          },
          {
            "name": "Eggplant",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?eggplant,vegetables"
          },
          {
            "name": "Artichokes",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?artichokes,vegetables"
          },
          {
            "name": "Asparagus",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?asparagus,vegetables"
          },
          {
            "name": "Green beans",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?greenbeans,vegetables"
          },
          {
            "name": "Peas",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?peas,vegetables"
          },
          {
            "name": "Corn",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?corn,vegetables"
          },
          {
            "name": "Beets",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?beets,vegetables"
          },
          {
            "name": "Pumpkin",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?pumpkin,vegetables"
          },
          {
            "name": "Sweet potato",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?sweetpotato,vegetables"
          }
        ]
      },
      {
        "name": "Meat",
        "items": [
          {
            "name": "Beef",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?beef,meat"
          },
          {
            "name": "Chicken",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?chicken,meat"
          },
          {
            "name": "Pork",
            "price": 8.49,
            "image": "https://source.unsplash.com/800x600/?pork,meat"
          },
          {
            "name": "Lamb",
            "price": 11.99,
            "image": "https://source.unsplash.com/800x600/?lamb,meat"
          }
        ]
      },
      {
        "name": "Seafood",
        "items": [
          {
            "name": "Salmon",
            "price": 12.99,
            "image": "https://source.unsplash.com/800x600/?salmon,seafood"
          },
          {
            "name": "Shrimp",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?shrimp,seafood"
          },
          {
            "name": "Crab",
            "price": 16.99,
            "image": "https://source.unsplash.com/800x600/?crab,seafood"
          },
          {
            "name": "Lobster",
            "price": 32.99,
            "image": "https://source.unsplash.com/800x600/?lobster,seafood"
          }
        ]
      },
      {
        "name": "Dairy",
        "items": [
          {
            "name": "Milk",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?milk,dairy"
          },
          {
            "name": "Cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cheese,dairy"
          },
          {
            "name": "Butter",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?butter,dairy"
          },
          {
            "name": "Yogurt",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?yogurt,dairy"
          },
          {
            "name": "Cream",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?cream,dairy"
          }
        ]
      },
      {
        "name": "Bakery",
        "items": [
          {
            "name": "Bread",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?bread,bakery"
          },
          {
            "name": "Croissant",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?croissant,bakery"
          },
          {
            "name": "Muffin",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?muffin,bakery"
          },
          {
            "name": "Bagel",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?bagel,bakery"
          },
          {
            "name": "Cake",
            "price": 15.99,
            "image": "https://source.unsplash.com/800x600/?cake,bakery"
          }
        ]
      }
    ]
  },
  {
    "name": "Fresh Market",
    "image": "https://www.instacart.com/image-server/100x/www.instacart.com/assets/domains/warehouse/logo/5/65f2304b-908e-4cd0-981d-0d4e4effa8de.png",
    "categories": [
      {
        "name": "Produce",
        "items": [
          {
            "name": "Apples",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?apples,produce"
          },
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,produce"
          },
          {
            "name": "Carrots",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?carrots,produce"
          },
          {
            "name": "Lettuce",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lettuce,produce"
          },
          {
            "name": "Tomatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?tomatoes,produce"
          },
          {
            "name": "Cucumbers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cucumbers,produce"
          },
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,produce"
          },
          {
            "name": "Grapes",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?grapes,produce"
          },
          {
            "name": "Peppers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?peppers,produce"
          },
          {
            "name": "Oranges",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?oranges,produce"
          },
          {
            "name": "Onions",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?onions,produce"
          },
          {
            "name": "Potatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,produce"
          },
          {
            "name": "Avocados",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?avocados,produce"
          },
          {
            "name": "Strawberries",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?strawberries,produce"
          },
          {
            "name": "Mushrooms",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mushrooms,produce"
          },
          {
            "name": "Pineapple",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?pineapple,produce"
          },
          {
            "name": "Kale",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?kale,produce"
          },
          {
            "name": "Cabbage",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cabbage,produce"
          },
          {
            "name": "Corn",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?corn,produce"
          },
          {
            "name": "Zucchini",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?zucchini,produce"
          }
        ]
      },
      {
        "name": "Meat",
        "items": [
          {
            "name": "Beef",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?beef,meat"
          },
          {
            "name": "Chicken",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?chicken,meat"
          },
          {
            "name": "Pork",
            "price": 7.99,
            "image": "https://source.unsplash.com/800x600/?pork,meat"
          },
          {
            "name": "Lamb",
            "price": 12.99,
            "image": "https://source.unsplash.com/800x600/?lamb,meat"
          },
          {
            "name": "Salmon",
            "price": 10.99,
            "image": "https://source.unsplash.com/800x600/?salmon,meat"
          },
          {
            "name": "Shrimp",
            "price": 8.99,
            "image": "https://source.unsplash.com/800x600/?shrimp,meat"
          },
          {
            "name": "Ground beef",
            "price": 8.99,
            "image": "https://source.unsplash.com/800x600/?ground-beef,meat"
          },
          {
            "name": "Tuna",
            "price": 11.99,
            "image": "https://source.unsplash.com/800x600/?tuna,meat"
          },
          {
            "name": "Crab",
            "price": 14.99,
            "image": "https://source.unsplash.com/800x600/?crab,meat"
          },
          {
            "name": "Cod",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?cod,meat"
          },
          {
            "name": "Turkey",
            "price": 7.99,
            "image": "https://source.unsplash.com/800x600/?turkey,meat"
          },
          {
            "name": "Halibut",
            "price": 15.99,
            "image": "https://source.unsplash.com/800x600/?halibut,meat"
          },
          {
            "name": "Sausage",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?sausage,meat"
          },
          {
            "name": "Bacon",
            "price": 5.99,
            "image": "https://source.unsplash.com/800x600/?bacon,meat"
          },
          {
            "name": "Lobster",
            "price": 25.99,
            "image": "https://source.unsplash.com/800x600/?lobster,meat"
          },
          {
            "name": "Veal",
            "price": 14.99,
            "image": "https://source.unsplash.com/800x600/?veal,meat"
          },
          {
            "name": "Duck",
            "price": 13.99,
            "image": "https://source.unsplash.com/800x600/?duck,meat"
          },
          {
            "name": "Pheasant",
            "price": 18.99,
            "image": "https://source.unsplash.com/800x600/?pheasant,meat"
          },
          {
            "name": "Quail",
            "price": 17.99,
            "image": "https://source.unsplash.com/800x600/?quail,meat"
          }
        ]
      },
      {
        "name": "Bakery",
        "items": [
          {
            "name": "Bread",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?bread,bakery"
          },
          {
            "name": "Croissant",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?croissant,bakery"
          },
          {
            "name": "Bagel",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?bagel,bakery"
          },
          {
            "name": "Muffin",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?muffin,bakery"
          },
          {
            "name": "Pastry",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?pastry,bakery"
          },
          {
            "name": "Cupcake",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?cupcake,bakery"
          },
          {
            "name": "Cake",
            "price": 15.99,
            "image": "https://source.unsplash.com/800x600/?cake,bakery"
          },
          {
            "name": "Pie",
            "price": 12.99,
            "image": "https://source.unsplash.com/800x600/?pie,bakery"
          },
          {
            "name": "Donut",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?donut,bakery"
          },
          {
            "name": "Baguette",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?baguette,bakery"
          },
          {
            "name": "Sourdough bread",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?sourdough-bread,bakery"
          },
          {
            "name": "Pretzel",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?pretzel,bakery"
          },
          {
            "name": "Danish",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?danish,bakery"
          },
          {
            "name": "Focaccia",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?focaccia,bakery"
          },
          {
            "name": "Cinnamon roll",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?cinnamon-roll,bakery"
          },
          {
            "name": "Brioche",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?brioche,bakery"
          },
          {
            "name": "English muffin",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?english-muffin,bakery"
          },
          {
            "name": "Cornbread",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?cornbread,bakery"
          },
          {
            "name": "Scone",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?scone,bakery"
          },
          {
            "name": "Ciabatta",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?ciabatta,bakery"
          }
        ]
      },
      {
        "name": "Dairy",
        "items": [
          {
            "name": "Milk",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?milk,dairy"
          },
          {
            "name": "Butter",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?butter,dairy"
          },
          {
            "name": "Cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cheese,dairy"
          },
          {
            "name": "Yogurt",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?yogurt,dairy"
          },
          {
            "name": "Sour cream",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?sour-cream,dairy"
          },
          {
            "name": "Cream cheese",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?cream-cheese,dairy"
          },
          {
            "name": "Eggs",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?eggs,dairy"
          },
          {
            "name": "Whipping cream",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?whipping-cream,dairy"
          },
          {
            "name": "Half and half",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?half-and-half,dairy"
          },
          {
            "name": "Margarine",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?margarine,dairy"
          },
          {
            "name": "Cottage cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cottage-cheese,dairy"
          },
          {
            "name": "Almond milk",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?almond-milk,dairy"
          },
          {
            "name": "Soy milk",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?soy-milk,dairy"
          },
          {
            "name": "Heavy cream",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?heavy-cream,dairy"
          },
          {
            "name": "Goat cheese",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?goat-cheese,dairy"
          },
          {
            "name": "Feta cheese",
            "price": 5.99,
            "image": "https://source.unsplash.com/800x600/?feta-cheese,dairy"
          },
          {
            "name": "Buttermilk",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?buttermilk,dairy"
          },
          {
            "name": "Cottage cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cottage-cheese,dairy"
          },
          {
            "name": "Greek yogurt",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?greek-yogurt,dairy"
          },
          {
            "name": "Buttercream",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?buttercream,dairy"
          }
        ]
      },
      {
        "name": "Produce",
        "items": [
          {
            "name": "Apples",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?apples,produce"
          },
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,produce"
          },
          {
            "name": "Carrots",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?carrots,produce"
          },
          {
            "name": "Oranges",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?oranges,produce"
          },
          {
            "name": "Lemons",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?lemons,produce"
          },
          {
            "name": "Kale",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?kale,produce"
          },
          {
            "name": "Lettuce",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lettuce,produce"
          },
          {
            "name": "Spinach",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?spinach,produce"
          },
          {
            "name": "Tomatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?tomatoes,produce"
          },
          {
            "name": "Cucumbers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cucumbers,produce"
          },
          {
            "name": "Bell peppers",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?bell-peppers,produce"
          },
          {
            "name": "Zucchini",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?zucchini,produce"
          },
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,produce"
          },
          {
            "name": "Cauliflower",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?cauliflower,produce"
          },
          {
            "name": "Corn",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?corn,produce"
          },
          {
            "name": "Green beans",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?green-beans,produce"
          },
          {
            "name": "Onions",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?onions,produce"
          },
          {
            "name": "Potatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,produce"
          },
          {
            "name": "Sweet potatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?sweet-potatoes,produce"
          }
        ]
      }
    ]
  },
  {
    "name": "Green Grocers",
    "image": "https://www.instacart.com/image-server/100x/www.instacart.com/assets/domains/warehouse/logo/436/695aaa61-4dab-4965-a7e3-0c2f93e9b119.png",
    "categories": [
      {
        "name": "Vegetables",
        "items": [
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,vegetables"
          },
          {
            "name": "Carrots",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?carrots,vegetables"
          },
          {
            "name": "Cauliflower",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?cauliflower,vegetables"
          },
          {
            "name": "Spinach",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?spinach,vegetables"
          },
          {
            "name": "Kale",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?kale,vegetables"
          },
          {
            "name": "Lettuce",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lettuce,vegetables"
          },
          {
            "name": "Cabbage",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cabbage,vegetables"
          },
          {
            "name": "Peppers",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?peppers,vegetables"
          },
          {
            "name": "Tomatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?tomatoes,vegetables"
          },
          {
            "name": "Zucchini",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?zucchini,vegetables"
          },
          {
            "name": "Onions",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?onions,vegetables"
          },
          {
            "name": "Garlic",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?garlic,vegetables"
          },
          {
            "name": "Ginger",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?ginger,vegetables"
          },
          {
            "name": "Mushrooms",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mushrooms,vegetables"
          },
          {
            "name": "Potatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,vegetables"
          },
          {
            "name": "Sweet potatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?sweet-potatoes,vegetables"
          }
        ]
      },
      {
        "name": "Fruits",
        "items": [
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,fruits"
          },
          {
            "name": "Apples",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?apples,fruits"
          },
          {
            "name": "Oranges",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?oranges,fruits"
          },
          {
            "name": "Grapes",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?grapes,fruits"
          },
          {
            "name": "Lemons",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lemons,fruits"
          },
          {
            "name": "Limes",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?limes,fruits"
          },
          {
            "name": "Peaches",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?peaches,fruits"
          },
          {
            "name": "Pineapples",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?pineapples,fruits"
          },
          {
            "name": "Strawberries",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?strawberries,fruits"
          },
          {
            "name": "Blueberries",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?blueberries,fruits"
          },
          {
            "name": "Raspberries",
            "price": 4.49,
            "image": "https://source.unsplash.com/800x600/?raspberries,fruits"
          },
          {
            "name": "Watermelon",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?watermelon,fruits"
          },
          {
            "name": "Honeydew",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?honeydew,fruits"
          },
          {
            "name": "Mangoes",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mangoes,fruits"
          },
          {
            "name": "Pears",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?pears,fruits"
          },
          {
            "name": "Plums",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?plums,fruits"
          },
          {
            "name": "Cherries",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?cherries,fruits"
          },
          {
            "name": "Kiwi",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?kiwi,fruits"
          }
        ]
      },
      {
        "name": "Bakery",
        "items": [
          {
            "name": "Bread",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?bread,bakery"
          },
          {
            "name": "Croissant",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?croissant,bakery"
          },
          {
            "name": "Bagels",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?bagels,bakery"
          },
          {
            "name": "Donuts",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?donuts,bakery"
          },
          {
            "name": "Muffins",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?muffins,bakery"
          },
          {
            "name": "Cupcakes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cupcakes,bakery"
          }
        ]
      }
    ]
  },
  {
    "name": "My Groceries",
    "image": "https://www.instacart.com/image-server/100x/www.instacart.com/assets/domains/warehouse/logo/1/1fe0065e-a947-4b5d-b274-3900694536d5.png",
    "categories": [
      {
        "name": "Fruits",
        "items": [
          {
            "name": "Apples",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?apples,fruit"
          },
          {
            "name": "Oranges",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?oranges,fruit"
          },
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,fruit"
          },
          {
            "name": "Pineapples",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?pineapples,fruit"
          },
          {
            "name": "Strawberries",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?strawberries,fruit"
          },
          {
            "name": "Grapes",
            "price": 2.79,
            "image": "https://source.unsplash.com/800x600/?grapes,fruit"
          },
          {
            "name": "Blueberries",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?blueberries,fruit"
          },
          {
            "name": "Raspberries",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?raspberries,fruit"
          },
          {
            "name": "Mangoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?mangoes,fruit"
          },
          {
            "name": "Pears",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?pears,fruit"
          },
          {
            "name": "Watermelons",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?watermelons,fruit"
          },
          {
            "name": "Cantaloupes",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cantaloupes,fruit"
          },
          {
            "name": "Honeydews",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?honeydews,fruit"
          },
          {
            "name": "Lemons",
            "price": 1.29,
            "image": "https://source.unsplash.com/800x600/?lemons,fruit"
          },
          {
            "name": "Limes",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?limes,fruit"
          },
          {
            "name": "Grapefruits",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?grapefruits,fruit"
          },
          {
            "name": "Kiwi",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?kiwi,fruit"
          },
          {
            "name": "Plums",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?plums,fruit"
          },
          {
            "name": "Nectarines",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?nectarines,fruit"
          },
          {
            "name": "Apricots",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?apricots,fruit"
          },
          {
            "name": "Clementines",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?clementines,fruit"
          }
        ]
      },
      {
        "name": "Vegetables",
        "items": [
          {
            "name": "Carrots",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?carrots,vegetables"
          },
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,vegetables"
          },
          {
            "name": "Cauliflower",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?cauliflower,vegetables"
          },
          {
            "name": "Tomatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?tomatoes,vegetables"
          },
          {
            "name": "Lettuce",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?lettuce,vegetables"
          },
          {
            "name": "Spinach",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?spinach,vegetables"
          },
          {
            "name": "Kale",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?kale,vegetables"
          },
          {
            "name": "Peppers",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?peppers,vegetables"
          },
          {
            "name": "Potatoes",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,vegetables"
          },
          {
            "name": "Onions",
            "price": 1.29,
            "image": "https://source.unsplash.com/800x600/?onions,vegetables"
          },
          {
            "name": "Garlic",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?garlic,vegetables"
          },
          {
            "name": "Mushrooms",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mushrooms,vegetables"
          },
          {
            "name": "Cucumbers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cucumbers,vegetables"
          },
          {
            "name": "Zucchini",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?zucchini,vegetables"
          },
          {
            "name": "Eggplant",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?eggplant,vegetables"
          },
          {
            "name": "Artichokes",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?artichokes,vegetables"
          },
          {
            "name": "Asparagus",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?asparagus,vegetables"
          },
          {
            "name": "Green beans",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?greenbeans,vegetables"
          },
          {
            "name": "Peas",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?peas,vegetables"
          },
          {
            "name": "Corn",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?corn,vegetables"
          },
          {
            "name": "Beets",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?beets,vegetables"
          },
          {
            "name": "Pumpkin",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?pumpkin,vegetables"
          },
          {
            "name": "Sweet potato",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?sweetpotato,vegetables"
          }
        ]
      },
      {
        "name": "Meat",
        "items": [
          {
            "name": "Beef",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?beef,meat"
          },
          {
            "name": "Chicken",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?chicken,meat"
          },
          {
            "name": "Pork",
            "price": 8.49,
            "image": "https://source.unsplash.com/800x600/?pork,meat"
          },
          {
            "name": "Lamb",
            "price": 11.99,
            "image": "https://source.unsplash.com/800x600/?lamb,meat"
          }
        ]
      },
      {
        "name": "Seafood",
        "items": [
          {
            "name": "Salmon",
            "price": 12.99,
            "image": "https://source.unsplash.com/800x600/?salmon,seafood"
          },
          {
            "name": "Shrimp",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?shrimp,seafood"
          },
          {
            "name": "Crab",
            "price": 16.99,
            "image": "https://source.unsplash.com/800x600/?crab,seafood"
          },
          {
            "name": "Lobster",
            "price": 32.99,
            "image": "https://source.unsplash.com/800x600/?lobster,seafood"
          }
        ]
      },
      {
        "name": "Dairy",
        "items": [
          {
            "name": "Milk",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?milk,dairy"
          },
          {
            "name": "Cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cheese,dairy"
          },
          {
            "name": "Butter",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?butter,dairy"
          },
          {
            "name": "Yogurt",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?yogurt,dairy"
          },
          {
            "name": "Cream",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?cream,dairy"
          }
        ]
      },
      {
        "name": "Bakery",
        "items": [
          {
            "name": "Bread",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?bread,bakery"
          },
          {
            "name": "Croissant",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?croissant,bakery"
          },
          {
            "name": "Muffin",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?muffin,bakery"
          },
          {
            "name": "Bagel",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?bagel,bakery"
          },
          {
            "name": "Cake",
            "price": 15.99,
            "image": "https://source.unsplash.com/800x600/?cake,bakery"
          }
        ]
      }
    ]
  },
  {
    "name": "Fresh Market",
    "image": "https://www.instacart.com/image-server/100x/www.instacart.com/assets/domains/warehouse/logo/5/65f2304b-908e-4cd0-981d-0d4e4effa8de.png",
    "categories": [
      {
        "name": "Produce",
        "items": [
          {
            "name": "Apples",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?apples,produce"
          },
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,produce"
          },
          {
            "name": "Carrots",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?carrots,produce"
          },
          {
            "name": "Lettuce",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lettuce,produce"
          },
          {
            "name": "Tomatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?tomatoes,produce"
          },
          {
            "name": "Cucumbers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cucumbers,produce"
          },
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,produce"
          },
          {
            "name": "Grapes",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?grapes,produce"
          },
          {
            "name": "Peppers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?peppers,produce"
          },
          {
            "name": "Oranges",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?oranges,produce"
          },
          {
            "name": "Onions",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?onions,produce"
          },
          {
            "name": "Potatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,produce"
          },
          {
            "name": "Avocados",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?avocados,produce"
          },
          {
            "name": "Strawberries",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?strawberries,produce"
          },
          {
            "name": "Mushrooms",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mushrooms,produce"
          },
          {
            "name": "Pineapple",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?pineapple,produce"
          },
          {
            "name": "Kale",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?kale,produce"
          },
          {
            "name": "Cabbage",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cabbage,produce"
          },
          {
            "name": "Corn",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?corn,produce"
          },
          {
            "name": "Zucchini",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?zucchini,produce"
          }
        ]
      },
      {
        "name": "Meat",
        "items": [
          {
            "name": "Beef",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?beef,meat"
          },
          {
            "name": "Chicken",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?chicken,meat"
          },
          {
            "name": "Pork",
            "price": 7.99,
            "image": "https://source.unsplash.com/800x600/?pork,meat"
          },
          {
            "name": "Lamb",
            "price": 12.99,
            "image": "https://source.unsplash.com/800x600/?lamb,meat"
          },
          {
            "name": "Salmon",
            "price": 10.99,
            "image": "https://source.unsplash.com/800x600/?salmon,meat"
          },
          {
            "name": "Shrimp",
            "price": 8.99,
            "image": "https://source.unsplash.com/800x600/?shrimp,meat"
          },
          {
            "name": "Ground beef",
            "price": 8.99,
            "image": "https://source.unsplash.com/800x600/?ground-beef,meat"
          },
          {
            "name": "Tuna",
            "price": 11.99,
            "image": "https://source.unsplash.com/800x600/?tuna,meat"
          },
          {
            "name": "Crab",
            "price": 14.99,
            "image": "https://source.unsplash.com/800x600/?crab,meat"
          },
          {
            "name": "Cod",
            "price": 9.99,
            "image": "https://source.unsplash.com/800x600/?cod,meat"
          },
          {
            "name": "Turkey",
            "price": 7.99,
            "image": "https://source.unsplash.com/800x600/?turkey,meat"
          },
          {
            "name": "Halibut",
            "price": 15.99,
            "image": "https://source.unsplash.com/800x600/?halibut,meat"
          },
          {
            "name": "Sausage",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?sausage,meat"
          },
          {
            "name": "Bacon",
            "price": 5.99,
            "image": "https://source.unsplash.com/800x600/?bacon,meat"
          },
          {
            "name": "Lobster",
            "price": 25.99,
            "image": "https://source.unsplash.com/800x600/?lobster,meat"
          },
          {
            "name": "Veal",
            "price": 14.99,
            "image": "https://source.unsplash.com/800x600/?veal,meat"
          },
          {
            "name": "Duck",
            "price": 13.99,
            "image": "https://source.unsplash.com/800x600/?duck,meat"
          },
          {
            "name": "Pheasant",
            "price": 18.99,
            "image": "https://source.unsplash.com/800x600/?pheasant,meat"
          },
          {
            "name": "Quail",
            "price": 17.99,
            "image": "https://source.unsplash.com/800x600/?quail,meat"
          }
        ]
      },
      {
        "name": "Bakery",
        "items": [
          {
            "name": "Bread",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?bread,bakery"
          },
          {
            "name": "Croissant",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?croissant,bakery"
          },
          {
            "name": "Bagel",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?bagel,bakery"
          },
          {
            "name": "Muffin",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?muffin,bakery"
          },
          {
            "name": "Pastry",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?pastry,bakery"
          },
          {
            "name": "Cupcake",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?cupcake,bakery"
          },
          {
            "name": "Cake",
            "price": 15.99,
            "image": "https://source.unsplash.com/800x600/?cake,bakery"
          },
          {
            "name": "Pie",
            "price": 12.99,
            "image": "https://source.unsplash.com/800x600/?pie,bakery"
          },
          {
            "name": "Donut",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?donut,bakery"
          },
          {
            "name": "Baguette",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?baguette,bakery"
          },
          {
            "name": "Sourdough bread",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?sourdough-bread,bakery"
          },
          {
            "name": "Pretzel",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?pretzel,bakery"
          },
          {
            "name": "Danish",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?danish,bakery"
          },
          {
            "name": "Focaccia",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?focaccia,bakery"
          },
          {
            "name": "Cinnamon roll",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?cinnamon-roll,bakery"
          },
          {
            "name": "Brioche",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?brioche,bakery"
          },
          {
            "name": "English muffin",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?english-muffin,bakery"
          },
          {
            "name": "Cornbread",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?cornbread,bakery"
          },
          {
            "name": "Scone",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?scone,bakery"
          },
          {
            "name": "Ciabatta",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?ciabatta,bakery"
          }
        ]
      },
      {
        "name": "Dairy",
        "items": [
          {
            "name": "Milk",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?milk,dairy"
          },
          {
            "name": "Butter",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?butter,dairy"
          },
          {
            "name": "Cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cheese,dairy"
          },
          {
            "name": "Yogurt",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?yogurt,dairy"
          },
          {
            "name": "Sour cream",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?sour-cream,dairy"
          },
          {
            "name": "Cream cheese",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?cream-cheese,dairy"
          },
          {
            "name": "Eggs",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?eggs,dairy"
          },
          {
            "name": "Whipping cream",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?whipping-cream,dairy"
          },
          {
            "name": "Half and half",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?half-and-half,dairy"
          },
          {
            "name": "Margarine",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?margarine,dairy"
          },
          {
            "name": "Cottage cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cottage-cheese,dairy"
          },
          {
            "name": "Almond milk",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?almond-milk,dairy"
          },
          {
            "name": "Soy milk",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?soy-milk,dairy"
          },
          {
            "name": "Heavy cream",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?heavy-cream,dairy"
          },
          {
            "name": "Goat cheese",
            "price": 6.99,
            "image": "https://source.unsplash.com/800x600/?goat-cheese,dairy"
          },
          {
            "name": "Feta cheese",
            "price": 5.99,
            "image": "https://source.unsplash.com/800x600/?feta-cheese,dairy"
          },
          {
            "name": "Buttermilk",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?buttermilk,dairy"
          },
          {
            "name": "Cottage cheese",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?cottage-cheese,dairy"
          },
          {
            "name": "Greek yogurt",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?greek-yogurt,dairy"
          },
          {
            "name": "Buttercream",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?buttercream,dairy"
          }
        ]
      },
      {
        "name": "Produce",
        "items": [
          {
            "name": "Apples",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?apples,produce"
          },
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,produce"
          },
          {
            "name": "Carrots",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?carrots,produce"
          },
          {
            "name": "Oranges",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?oranges,produce"
          },
          {
            "name": "Lemons",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?lemons,produce"
          },
          {
            "name": "Kale",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?kale,produce"
          },
          {
            "name": "Lettuce",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lettuce,produce"
          },
          {
            "name": "Spinach",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?spinach,produce"
          },
          {
            "name": "Tomatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?tomatoes,produce"
          },
          {
            "name": "Cucumbers",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cucumbers,produce"
          },
          {
            "name": "Bell peppers",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?bell-peppers,produce"
          },
          {
            "name": "Zucchini",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?zucchini,produce"
          },
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,produce"
          },
          {
            "name": "Cauliflower",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?cauliflower,produce"
          },
          {
            "name": "Corn",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?corn,produce"
          },
          {
            "name": "Green beans",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?green-beans,produce"
          },
          {
            "name": "Onions",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?onions,produce"
          },
          {
            "name": "Potatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,produce"
          },
          {
            "name": "Sweet potatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?sweet-potatoes,produce"
          }
        ]
      }
    ]
  },
  {
    "name": "Green Grocers",
    "image": "https://www.instacart.com/image-server/100x/www.instacart.com/assets/domains/warehouse/logo/436/695aaa61-4dab-4965-a7e3-0c2f93e9b119.png",
    "categories": [
      {
        "name": "Vegetables",
        "items": [
          {
            "name": "Broccoli",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?broccoli,vegetables"
          },
          {
            "name": "Carrots",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?carrots,vegetables"
          },
          {
            "name": "Cauliflower",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?cauliflower,vegetables"
          },
          {
            "name": "Spinach",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?spinach,vegetables"
          },
          {
            "name": "Kale",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?kale,vegetables"
          },
          {
            "name": "Lettuce",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lettuce,vegetables"
          },
          {
            "name": "Cabbage",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cabbage,vegetables"
          },
          {
            "name": "Peppers",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?peppers,vegetables"
          },
          {
            "name": "Tomatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?tomatoes,vegetables"
          },
          {
            "name": "Zucchini",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?zucchini,vegetables"
          },
          {
            "name": "Onions",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?onions,vegetables"
          },
          {
            "name": "Garlic",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?garlic,vegetables"
          },
          {
            "name": "Ginger",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?ginger,vegetables"
          },
          {
            "name": "Mushrooms",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mushrooms,vegetables"
          },
          {
            "name": "Potatoes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?potatoes,vegetables"
          },
          {
            "name": "Sweet potatoes",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?sweet-potatoes,vegetables"
          }
        ]
      },
      {
        "name": "Fruits",
        "items": [
          {
            "name": "Bananas",
            "price": 0.99,
            "image": "https://source.unsplash.com/800x600/?bananas,fruits"
          },
          {
            "name": "Apples",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?apples,fruits"
          },
          {
            "name": "Oranges",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?oranges,fruits"
          },
          {
            "name": "Grapes",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?grapes,fruits"
          },
          {
            "name": "Lemons",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?lemons,fruits"
          },
          {
            "name": "Limes",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?limes,fruits"
          },
          {
            "name": "Peaches",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?peaches,fruits"
          },
          {
            "name": "Pineapples",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?pineapples,fruits"
          },
          {
            "name": "Strawberries",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?strawberries,fruits"
          },
          {
            "name": "Blueberries",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?blueberries,fruits"
          },
          {
            "name": "Raspberries",
            "price": 4.49,
            "image": "https://source.unsplash.com/800x600/?raspberries,fruits"
          },
          {
            "name": "Watermelon",
            "price": 4.99,
            "image": "https://source.unsplash.com/800x600/?watermelon,fruits"
          },
          {
            "name": "Honeydew",
            "price": 3.99,
            "image": "https://source.unsplash.com/800x600/?honeydew,fruits"
          },
          {
            "name": "Mangoes",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?mangoes,fruits"
          },
          {
            "name": "Pears",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?pears,fruits"
          },
          {
            "name": "Plums",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?plums,fruits"
          },
          {
            "name": "Cherries",
            "price": 3.49,
            "image": "https://source.unsplash.com/800x600/?cherries,fruits"
          },
          {
            "name": "Kiwi",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?kiwi,fruits"
          }
        ]
      },
      {
        "name": "Bakery",
        "items": [
          {
            "name": "Bread",
            "price": 2.99,
            "image": "https://source.unsplash.com/800x600/?bread,bakery"
          },
          {
            "name": "Croissant",
            "price": 1.49,
            "image": "https://source.unsplash.com/800x600/?croissant,bakery"
          },
          {
            "name": "Bagels",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?bagels,bakery"
          },
          {
            "name": "Donuts",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?donuts,bakery"
          },
          {
            "name": "Muffins",
            "price": 2.49,
            "image": "https://source.unsplash.com/800x600/?muffins,bakery"
          },
          {
            "name": "Cupcakes",
            "price": 1.99,
            "image": "https://source.unsplash.com/800x600/?cupcakes,bakery"
          }
        ]
      }
    ]
  }
]