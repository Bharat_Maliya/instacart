import './Header.scss'
import { Cart, Hamburger, Location } from '../../../assets/Icones/Icons'
import { images } from '../../../assets/Images/Images'
import Auth from '../Auth/Auth'

import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { selectCurrentUser } from '../../../redux/user/userSelector'
import Button from '../../Molecules/Button/Button'
import { auth } from '../../../firebase/firebaseCongig'
import SearchBar from '../../Molecules/SearchBar/SearchBar'

const Header = ({ user }) => {
  return (
    <nav className='d-flex-space-btwn gap-10 Header p'>
      <div className='d-flex-align gap-10'>
        <Hamburger />

        <img src={images.LOGO} alt='name' />
      </div>
      <div style={{ width: '70%' }}>
        <SearchBar />

      </div>
      <div>
        <Location />
      </div>
      <div className='d-flex-align gap-10'>
        {!user ? <> <Auth /> <Cart /> </> :
          <>
            <h3>
              {user.displayName}
            </h3>
            <Button onClick={() => auth.signOut()} text='Logout' />
          </>
        }
      </div>
    </nav>
  )
}

const mapStateToProps = createStructuredSelector({
  user: selectCurrentUser
})

export default connect(mapStateToProps)(Header);