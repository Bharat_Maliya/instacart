import './Carousel.scss'
import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css"

const Carousel = ({ className, children }) => {


  const settings = {
    // dots: true,
    // infinite: true,
    // speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,

  };
  let slide = Math.floor(window.innerWidth / 420);
  return (
    <div className={`insta-carousel ${className}`}>
      <Slider {...settings}>
        {children}
      </Slider>
    </div>
  );
};

export default Carousel;
