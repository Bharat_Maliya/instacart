import { useState } from 'react'
import AuthPill from '../../Atoms/AuthPill/AuthPill'
import Input from '../../Atoms/Input/Input'
import Button from '../../Molecules/Button/Button'

import { auth, createUserProfileDocument, signInWithGoogle } from '../../../firebase/firebaseCongig'

import './Auth.scss'
import { Back, Google } from '../../../assets/Icones/Icons'


const Auth = () => {
  const [signUp, setSignUp] = useState({})
  const [authScreen, setAuthScreen] = useState(1)

  const handleChange = (e) => {
    const { name, value } = e.target
    setSignUp((prev) => {
      return {
        ...prev,
        [name]: value
      };
    });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const { displayName, email, password, confirmpassword } = signUp;

    if (password !== confirmpassword) {
      alert('password dose not match')
      return;
    }

    try {
      const { user } = await auth.createUserWithEmailAndPassword(email, password);
      await createUserProfileDocument(user, { displayName });
      setSignUp({ displayName: '', email: '', password: '', confirmpassword: '' })

    } catch (error) {
      console.error(error);
    }
  }

  const Login = async (e) => {
    e.preventDefault();
    const { email, password } = signUp;
    try {
      await auth.signInWithEmailAndPassword(email, password);
      setSignUp({ email: '', password: '' });
    } catch (error) {
      alert(error.message)
    }
  }

  return (
    <div className="d-flex-align gap-10 Auth">
      <AuthPill authScreen={authScreen} style={{ color: 'white', background: 'rgb(10, 173, 10)', padding: '15px 20px', borderRadius: '33px', fontWeight: 'bold', border: 'none' }} name='Login'
        children={
          <div className="auth-form">
            {
              authScreen === 2 ?
                <>
                  <span className='pointer' onClick={() => setAuthScreen(1)}> <Back /> </span>
                  <h1>Sign up</h1>
                  <form onSubmit={handleSubmit} className="d-flex-column gap-10">
                    <Input
                      label="Username"
                      type='text'
                      name='displayName'
                      value={signUp.displayName}
                      onChange={handleChange}
                    />
                    <Input
                      label="Email"
                      type='text'
                      name='email'
                      value={signUp.email}
                      onChange={handleChange}
                    />
                    <Input
                      label="Password"
                      type='password'
                      name='password'
                      value={signUp.password}
                      onChange={handleChange}
                    />
                    <Input
                      label="Confirm Password"
                      type='password'
                      name='confirmpassword'
                      value={signUp.confirmpassword}
                      onChange={handleChange}
                    />
                    <Button type='submit' text='Sign up' />
                  </form>
                  <div class="custom-line">
                    <div className="line"></div>
                    <div className="or">Or</div>
                    <div className="line"></div>
                  </div>
                  <Button icon={<><Google /> </>} bg={'white'} color={'black'} onClick={signInWithGoogle} text='Sign in with Google' />
                  <div className='dont-have'>
                    <p>Already have account</p>
                    <h3 onClick={() => setAuthScreen(1)}>Sign In</h3>
                  </div>
                </>
                :
                <>
                  <h1 className="auth-form-title" >Log in</h1>
                  <form onSubmit={Login} className="d-flex-column gap-10 abcd">
                    <Input
                      label="Email"
                      type='text'
                      name='email'
                      value={signUp.email}
                      onChange={handleChange}
                    />
                    <Input
                      label="Password"
                      type='password'
                      name='password'
                      value={signUp.password}
                      onChange={handleChange}
                    />
                    <Button type='submit' text='Login' />
                  </form>
                  <div class="custom-line">
                    <div className="line"></div>
                    <div className="or">Or</div>
                    <div className="line"></div>
                  </div>
                  <Button icon={<><Google /> </>} bg={'white'} color={'black'} onClick={signInWithGoogle} text='Sign in with Google' />
                  <div className='dont-have'>
                    <p>Don't have account</p>
                    <h3 onClick={() => setAuthScreen(2)}>Sign Up</h3>
                  </div>
                </>
            }
          </div>
        }
      />
    </div>
  );
}

export default Auth;

