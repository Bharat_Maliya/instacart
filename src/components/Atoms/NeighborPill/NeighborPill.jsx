import './NeighborPill.scss'

const NeighborPill = ({ img, name }) => {
  return (
    <div className='NeighborPill'>
      <img src={img ? img : 'https://reactnativecode.com/wp-content/uploads/2018/02/Default_Image_Thumbnail.png'} alt='name' />
      <p>{name} </p>
    </div>
  )
}

export default NeighborPill