import './Input.scss'

const Input = (props) => {
  return (
    <div className='d-flex gap-5 Input'>
      {props.label && <label {...props.name} >{props.label}</label>}
      <input {...props} />
    </div>
  )
}

export default Input