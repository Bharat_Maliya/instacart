import './AuthPill.scss'
import Modal from 'react-modal';
import React from 'react';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    borderRadius: '12px',
    borderColor: 'rgb(223, 220, 220)'
  },
};

const AuthPill = ({ ...props }) => {
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }
  return (
    <div className=' AuthPill'>
      <button onClick={openModal}{...props} >{props.name}</button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
      >
        <div className='auth-container'>
          {props.authScreen !== 2 && <button onClick={closeModal}>X</button>}
          {props.children}
        </div>
      </Modal>
    </div>
  );
}

export default AuthPill