import './ShopPill.scss'

const ShopPill = ({ name, img }) => {
  return (
    <div className='ShopPill'>
      <img src={img ? img : 'https://reactnativecode.com/wp-content/uploads/2018/02/Default_Image_Thumbnail.png'} alt='name' />
      <p>{name}</p>
    </div>
  )
}

export default ShopPill