import { Search } from '../../../assets/Icones/Icons'
import React from './SearchBar.scss'

const SearchBar = () => {
  return (
    <form className='SearchBar'>
      <input placeholder='Search products, stores and receipes' />
      <button type='submit' ><Search /></button>
    </form>
  )
}

export default SearchBar