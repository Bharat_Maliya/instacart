import './Button.scss'

const Button = (props) => {
  return (
    <button style={{ background: props.bg, color: props.color }} type={props.type} onClick={props.onClick} className='d-flex-center gap-10 Button'>{props.icon} {props.text}</button>
  )
}

export default Button