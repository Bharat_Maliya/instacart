import { RightChevron } from '../../../assets/Icones/Icons'
import './ShopCard.scss'

const ShopCard = ({ img, name, subName }) => {
  return (
    <div className='d-flex-space-btwn ShopCard'>
      <div className='d-flex-align gap-10 details'>
        <img src={img ? img : 'https://reactnativecode.com/wp-content/uploads/2018/02/Default_Image_Thumbnail.png'} alt='name' />
        <div className='detail'>
          <p>{name}</p>
          <small>{subName}</small>
        </div>
      </div>
      <RightChevron />
    </div>
  )
}

export default ShopCard