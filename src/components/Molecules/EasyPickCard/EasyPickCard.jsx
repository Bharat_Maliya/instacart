import './EasyPickCard.scss'

const EasyPickCard = () => {
  return (
    <div className='EasyPickCard'>
      <div class="details">
        <h2>
          Grocery
        </h2>
        <h3>
          By 5:45am
        </h3>
        <small className='small'>
          Free delivery with Instacart+
        </small>
        <ul >
          <li>
            <img src="https://www.instacart.com/image-server/32x/www.instacart.com/assets/domains/warehouse/logo/279/59eb00b1-2e19-4912-bf2e-0c7a1f71f8b1.png" srcset="https://www.instacart.com/image-server/48x/www.instacart.com/assets/domains/warehouse/logo/279/59eb00b1-2e19-4912-bf2e-0c7a1f71f8b1.png 1.5x, https://www.instacart.com/image-server/64x/www.instacart.com/assets/domains/warehouse/logo/279/59eb00b1-2e19-4912-bf2e-0c7a1f71f8b1.png 2x" alt="Sprouts Farmers Market" width="32" height="32" class="css-c7gbh3" />
          </li>
          <li>
            <img src="https://www.instacart.com/image-server/32x/www.instacart.com/assets/domains/warehouse/logo/1/1fe0065e-a947-4b5d-b274-3900694536d5.png" srcset="https://www.instacart.com/image-server/48x/www.instacart.com/assets/domains/warehouse/logo/1/1fe0065e-a947-4b5d-b274-3900694536d5.png 1.5x, https://www.instacart.com/image-server/64x/www.instacart.com/assets/domains/warehouse/logo/1/1fe0065e-a947-4b5d-b274-3900694536d5.png 2x" alt="Safeway" width="32" height="32" class="css-c7gbh3" />
          </li>
          <li>
            <img src="https://www.instacart.com/image-server/32x/www.instacart.com/assets/domains/warehouse/logo/5/65f2304b-908e-4cd0-981d-0d4e4effa8de.png" srcset="https://www.instacart.com/image-server/48x/www.instacart.com/assets/domains/warehouse/logo/5/65f2304b-908e-4cd0-981d-0d4e4effa8de.png 1.5x, https://www.instacart.com/image-server/64x/www.instacart.com/assets/domains/warehouse/logo/5/65f2304b-908e-4cd0-981d-0d4e4effa8de.png 2x" alt="Costco" width="32" height="32" class="css-c7gbh3" />
          </li>
          <li >
            <small className='badge'>
              +48
            </small>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default EasyPickCard