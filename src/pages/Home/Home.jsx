import { RightArrow, RightChevron } from '../../assets/Icones/Icons';
import NeighborPill from '../../components/Atoms/NeighborPill/NeighborPill';
import ShopPill from '../../components/Atoms/ShopPill/ShopPill';
import EasyPickCard from '../../components/Molecules/EasyPickCard/EasyPickCard';
import ShopCard from '../../components/Molecules/ShopCard/ShopCard';
import Carousel from '../../components/oraganisms/Carousel/Carousel';
import { shopdata } from '../../data/shops'

import './Home.scss'

const Home = () => {
  return (
    <div className='container Home'>

      <Carousel
        className='Home-shop-carousel'
        children={
          shopdata.map(item => {
            return (
              <ShopPill img={item.image} name={item.name} />
            )
          })
        }
      />

      <div className='shop-cards-container'>

        <div className='gap-20 shop-cards'>
          <EasyPickCard />
          <EasyPickCard />
          <EasyPickCard />
          <EasyPickCard />
          <EasyPickCard />
          <EasyPickCard />
        </div>

        <div className='d-flex-space-btwn shop-cards-title'>
          <button className='d-flex-center gap-5 show-more-btn'>Show More <RightChevron /></button>
        </div>
      </div>

      <div className='shop-cards-container'>
        <div className='d-flex-space-btwn shop-cards-title'>
          <h2>Stores to help you save</h2>
          <p className='d-flex-align gap-5 pointer b-500'>View All<RightArrow /> </p>
        </div>
        <div className='gap-20 shop-cards'>
          {shopdata.slice(0, 6).map(item => {
            return (
              <ShopCard img={item.image} name={item.name} subName={'shope is good to go'} />
            )
          })}
        </div>
      </div>

      <div className='shop-cards-container'>
        <div className='d-flex-space-btwn shop-cards-title'>
          <div>
            <h2>Neighborhood</h2>
            <small>Shop local</small>
          </div>
          <p className='d-flex-align gap-5 pointer b-500'>View All<RightArrow /> </p>
        </div>
        <div className='gap-20 d-flex'>
          <NeighborPill name={'jhbdxu'} />
          <NeighborPill name={'jhbdxu'} />
          <NeighborPill name={'jhbdxu'} />
          <NeighborPill name={'jhbdxu'} />
        </div>
      </div>

    </div>
  )
}

export default Home;