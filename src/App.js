import { useEffect, useState } from 'react'

import Header from './components/oraganisms/Header/Header';
import Home from './pages/Home/Home';

import { auth, createUserProfileDocument } from './firebase/firebaseCongig';
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect';
import { selectCurrentUser } from './redux/user/userSelector';
import { setCurrentUser } from "./redux/user/userAction";

import './App.scss';

function App({ currentUser, setCurrentUser }) {

  console.log(currentUser)

  let unsubscribeFromAuth = null

  useEffect(() => {

    unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      if (userAuth) {
        const userRef = await createUserProfileDocument(userAuth)
        userRef.onSnapshot(snapshot => {
          setCurrentUser({
            id: snapshot.id,
            ...snapshot.data()
          })
        })
      }
      else {
        setCurrentUser(userAuth);
      }
    });

    return (() => {
      unsubscribeFromAuth();
    })

  }, [])

  return (
    <div className="App">
      <Header />
      <Home />
      <h1>hello world</h1>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  currentUser: selectCurrentUser
});

const mapDispatchToProps = dispatch => (
  {
    setCurrentUser: user => dispatch(setCurrentUser(user))
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(App);
